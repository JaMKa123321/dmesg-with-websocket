const ws = new WebSocket('ws://localhost:8081');
const TYPES = ["emerg", "alert", "crit", "err", "warn", "notice", "info", "debug", "div"];

window.addEventListener("load", () => {
    for (var i = 0; i < 8; i++) {
        const elem = document.createElement("div");
        elem.id = "msg_" + TYPES[i];
        elem.style.background = (i % 2 == 0) ? "white" : "lightgray";
        elem.style.display = "none";

        const divTitle = document.createElement("p");
        divTitle.style.background = elem.style.background;
        divTitle.style.cursor = "pointer";
        divTitle.innerHTML = TYPES[i];
        divTitle.onclick = function() {
            elem.style.display = (elem.style.display == "none") ? "inline" : "none";
        };

        const ovalDiv = document.createElement("div");
        ovalDiv.style.background = elem.style.background;

        ovalDiv.appendChild(divTitle);
        ovalDiv.appendChild(elem);
        document.getElementById("msg_div").appendChild(ovalDiv);
        
    }
    console.log("Page is fully loaded");
});


ws.addEventListener("open", () => {
    console.log("Ready");
});

ws.addEventListener("message", (event) => {
    jsonRes = JSON.parse(event.data)["dmesg"];
    for (it of jsonRes) {
        const elem = document.createElement("div");
        elem.innerHTML = JSON.stringify(it);
        elem.style.background = "inherit";
        elem.style.border = "none";
        document.getElementById("msg_" + it["type"]).appendChild(elem);
    }
});

ws.addEventListener("error", () => {
    console.log("Error");
});