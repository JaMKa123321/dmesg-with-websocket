import asyncio, json
from websockets.server import serve

DMESG_KEYS = ['alert', 'crit', 'debug', 'emerg',
              'err', 'info', 'notice', 'warn']
info = []
length = 0

async def dmesgLoop():
    while(True):
        await asyncio.sleep(1)
        await cmdDmesg()

async def cmdDmesg():
    global info, length, DMESG_KEYS
    info.clear()
    for it in DMESG_KEYS:
        cmd = await asyncio.create_subprocess_shell(f'sudo dmesg -Jl {it}', stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE)
        ans, _ = await cmd.communicate()
        if (len(ans) == 0):
            continue
        jsonAns = json.loads(ans.decode())['dmesg']
        for arrItem in jsonAns:
            arrItem['type'] = it
        info.extend(jsonAns)
    length = len(info)

async def dmesg(websocket):
    global info, length
    currentLength = 0
    while(True):
        await asyncio.sleep(1)
        if (currentLength != length):
            await websocket.send(json.dumps({'dmesg':info}))
            currentLength = length

async def startSocket():
    server = await serve(dmesg, "localhost", 8081)
    await server.start_serving()

async def main():
    await asyncio.gather(startSocket(), dmesgLoop())

asyncio.run(main())