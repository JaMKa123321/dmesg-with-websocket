import asyncio, json, subprocess

# cmdRes = subprocess.run('sudo dmesg -Jl warn', shell=True, capture_output=True).stdout.decode()

# jsonCmdRes = json.loads(cmdRes)['dmesg']

# for it in jsonCmdRes:
#     it['type'] = 'warn'

# print(jsonCmdRes[2])

info = []
DMESG_KEYS = ['alert', 'crit', 'debug', 'emerg',
              'err', 'info', 'notice', 'warn']
length = 0

async def cmdDmesg():
    global info, length, DMESG_KEYS
    info.clear()
    for it in DMESG_KEYS:
        cmd = await asyncio.create_subprocess_shell(f'sudo dmesg -Jl {it}', stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE)
        ans, _ = await cmd.communicate()
        if (len(ans) == 0):
            continue
        jsonAns = json.loads(ans.decode())['dmesg']
        for arrItem in jsonAns:
            arrItem['type'] = it
        info.extend(jsonAns)
    length = len(info)
    for it in info:
        if it['type'] == 'warn':
            print(it)

asyncio.run(cmdDmesg())